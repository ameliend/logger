======================
Logger's Documentation
======================

.. image:: _static/images/logo.png

A custom **Logger** class that I use in most of my projects.

It has my color formatting, additional logging levels, a log file per day in the tempdir and also
a new feature that allows **Windows notifications**.

This **Logger** package provides a flexible logging system, with a pre-defined logger instance.

This allows me to either import a logging function directly:

.. code-block:: python

    from logger import info

    info("This is an informational message")


It's simple and fast.

Or I can use a custom logger name:

.. code-block:: python

    from logger import Logger

    logger = Logger("Demo")

    logger.info("This is an informational message")


✨ Features
------------

- My custom formatting

.. image:: _static/images/logger1.png

- New logging levels

    * TRACE (which adds the module, function and line number)
    * DONE
    * SUCCESS

- System notifications on Windows via toaster, using ``Notify=True``

.. image:: _static/images/notify.png

- Log uncaught exceptions

.. image:: _static/images/exceptions.png

See :ref:`usage` to see how to use the logger in your application.

.. toctree::
   :caption: 📖 Doc - Dev
   :maxdepth: 4

   usage

.. toctree::
   :caption: 📚 API
   :maxdepth: 3

   api
