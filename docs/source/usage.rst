Usage
=====

Root Logging
------------

You can directly import the logging functions to log in as **"root"**.

.. code-block:: python

    from logger import setLevel, trace, debug, info, done, warning, success, error, critical

    trace("This is a trace message")
    debug("This is a debug message")
    info("This is an info message")
    done("This is a done message")
    warning("This is a warning message")
    success("This is a success message")
    error("This is an error message")
    critical("This is a critical message")

    setLevel("WARNING") # Set the log level, TRACE by default

    trace("This is a trace message")  # This should not be logged
    debug("This is a debug message")  # This should not be logged
    info("This is an info message")  # This should not be logged
    done("This is a done message")  # This should not be logged
    warning("This is a warning message")
    success("This is a success message")
    error("This is an error message")
    critical("This is a critical message")

Output:

.. code-block:: bash

    15:43:09 :: ROOT :: <stdin>:<module>:1 :: TRACE :: This is a trace message
    15:43:09 :: ROOT :: DEBUG :: This is a debug message
    15:43:09 :: ROOT :: INFO :: This is an info message
    15:43:09 :: ROOT :: DONE :: This is a done message
    15:43:09 :: ROOT :: WARNING :: This is a warning message
    15:43:09 :: ROOT :: SUCCESS :: This is a success message
    15:43:09 :: ROOT :: ERROR :: This is an error message
    15:43:09 :: ROOT :: CRITICAL :: This is a critical message
    15:43:09 :: ROOT :: WARNING :: This is a warning message
    15:43:09 :: ROOT :: SUCCESS :: This is a success message
    15:43:09 :: ROOT :: ERROR :: This is an error message
    15:43:09 :: ROOT :: CRITICAL :: This is a critical message


Custom Logging
--------------

Custom Logger name
++++++++++++++++++

You can create as many logging instances as you like, each with its own name.

.. code-block:: python

    from logger import Logger

    # Create some logger instances
    main_logger = Logger(name="my_application", level="INFO")
    misc_logger = Logger(name="misc")

    main_logger.trace("This is a trace message")  # This should not be logged as the log level was set in "INFO" above.
    main_logger.debug("This is a debug message")  # This should not be logged as the log level was set in "INFO" above.
    main_logger.info("This is an info message")
    main_logger.done("This is a done message")
    main_logger.warn("This is a warning message")
    main_logger.success("This is a success message")
    main_logger.error("This is an error message")
    main_logger.critical("This is a critical message")
    main_logger.set_level("ERROR")
    main_logger.info("This is an info message")  # This should not be logged as the log level was set in "ERROR" above.
    misc_logger.debug("This is a trace message")
    misc_logger.debug("This is a debug message")

Output:

.. code-block:: bash

    15:43:09 :: my_application :: INFO :: This is an info message
    15:43:09 :: my_application :: DONE :: This is a done message
    15:43:09 :: my_application :: WARN :: This is a warning message
    15:43:09 :: my_application :: SUCCESS :: This is a success message
    15:43:09 :: my_application :: ERROR :: This is an error message
    15:43:09 :: my_application :: CRITICAL :: This is a critical message
    15:43:09 :: misc :: <stdin>:<module>:1 :: TRACE :: This is a trace message
    15:43:09 :: misc :: DEBUG :: This is a debug message

Custom Logger log file
++++++++++++++++++++++

You can also set a custom log file path.
By default, it is set to your **%temp%** dir location, with today's date and time.

.. code-block:: python

    from logger import Logger

    # Create a logger instance
    logger = Logger(name="my_application", log_file="/path/to/log_file.log")

Notify
------

You can display your log as a Windows notification, with the argument ``notify=True``.

.. note::

	Windows only.

.. code-block:: python

  info("An update is availible.", notify=True)

.. image:: _static/images/notify.png

Clicking on that notification will open the **log file**.
