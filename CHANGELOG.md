## [1.1.1](https://gitlab.com/ameliend/logger/compare/v1.1.0...v1.1.1) (2024-11-10)

### 🛠 Fixes

* **logger:** wrong import ([796f3dd](https://gitlab.com/ameliend/logger/commit/796f3dd1ab3e8ac0ef945e6d7229152fa1e2af5c))

## [1.1.0](https://gitlab.com/ameliend/logger/compare/v1.0.2...v1.1.0) (2024-11-10)

### 🚀 Features

* **cli:** added a cli entry point to open last log file ([a18024a](https://gitlab.com/ameliend/logger/commit/a18024a10490d6f9f1d939f7fa0642e7de7e81ec))

## [1.0.2](https://gitlab.com/ameliend/logger/compare/v1.0.1...v1.0.2) (2024-11-03)

### 🛠 Fixes

* **logger:** add utf-8 encoding to FileHandler ([674a5d7](https://gitlab.com/ameliend/logger/commit/674a5d704346b252a7baaff0fc906ac5725058c7))

## [1.0.1](https://gitlab.com/ameliend/logger/compare/v1.0.0...v1.0.1) (2024-11-01)

### 🛠 Fixes

* **notify:** correctly format log messages with arguments in notify_toaster ([1e6a105](https://gitlab.com/ameliend/logger/commit/1e6a10592b949eec84aefa3bb0c599e138b715da))

## [1.0.0](https://gitlab.com/ameliend/logger/compare/...v1.0.0) (2024-11-01)

### 📔 Docs

* update docs ([ea9c3e3](https://gitlab.com/ameliend/logger/commit/ea9c3e333459d901931c308f99bdf1e571869a83))

### 🦊 CI/CD

* **MANIFEST:** include resources ([476ea93](https://gitlab.com/ameliend/logger/commit/476ea9316cfbead17994a79ae36dc59a0300b089))
* **setup:** update the required Python version to 3.8, update classifiers ([aa26bc3](https://gitlab.com/ameliend/logger/commit/aa26bc3c10ed4ad464116367fcc7a85aab3747b6))

### 🧪 Tests

* added tests ([1167ee0](https://gitlab.com/ameliend/logger/commit/1167ee09ac6c3dc72f162d8c32701ab30e5a1dff))

### 🚀 Features

* initial commit ([d9abcfc](https://gitlab.com/ameliend/logger/commit/d9abcfc5cbb2b484f63404bdaab7f71879958f78))
* **logger:** initial commit ([9e59f11](https://gitlab.com/ameliend/logger/commit/9e59f11e3bfabd4aa9c11dbb0d087264812cde26))
