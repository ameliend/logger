[![coverage](https://gitlab.com/ameliend/logger/badges/main/coverage.svg)](https://gitlab.com/ameliend/logger/-/commits/main)
[![vscode-editor](https://badgen.net/badge/icon/visualstudio?icon=visualstudio&label)](https://code.visualstudio.com/)
[![uv](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/uv/main/assets/badge/v0.json)](https://github.com/astral-sh/uv)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)
[![Copier](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/copier-org/copier/master/img/badge/badge-grayscale-inverted-border-orange.json)](https://github.com/copier-org/copier)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![API Documentation](https://badgen.net/badge/icon/API%20documentation?icon=gitlab&label&color=cyan)](https://ameliend.gitlab.io/logger)

# Logger

<p align="center">
  <img src="./resources/logo.png">
</p>

A custom **Logger** class that I use in most of my projects.

It has my color formatting, additional logging levels, a log file per day in the tempdir and also
a new feature that allows **Windows notifications**.

This **Logger** package provides a flexible logging system, with a pre-defined logger instance.

This allows me to either import a logging function directly:

```python
from logger import info

info("This is an informational message")
```

It's simple and fast.

Or I can use a custom logger name:

```python
from logger import Logger

logger = Logger("Demo")

logger.info("This is an informational message")
```

## ✨ Features

- My custom formatting

<p align="center">
  <img src="./resources/logger1.png">
</p>

- New logging levels

    * TRACE (which adds the module, function and line number)
    * DONE
    * SUCCESS

- System notifications on Windows via toaster, using `Notify=True`

<p align="center">
  <img src="./resources/notify.png">
</p>

- Log uncaught exceptions

<p align="center">
  <img src="./resources/exceptions.png">
</p>

- Customizable log file path

See the [Gitlab Page documentation](https://ameliend.gitlab.io/logger/) for usage.
