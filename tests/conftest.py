"""Pytest configuration file for Logger."""

import pytest

from logger import Logger


@pytest.fixture
def test_logger():
    return Logger(name="Pytest")


@pytest.fixture
def test_logger_with_custom_filepath(tmp_path):
    return Logger(name="Pytest", log_file=tmp_path / "custom_logger_log_file.log")


@pytest.fixture
def test_message():
    return "This is a test."
