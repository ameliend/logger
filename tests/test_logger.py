"""Tests for Logger."""

from logger.main import LOG_FILE


def test_trace(capsys, test_logger, test_message):
    test_logger.trace(test_message)
    captured = capsys.readouterr()
    assert test_message in captured.err
    assert LOG_FILE.exists()
    with LOG_FILE.open() as f:
        lines = f.readlines()
    assert test_message in lines[-1]


def test_debug(capsys, test_logger, test_message):
    test_logger.debug(test_message)
    captured = capsys.readouterr()
    assert test_message in captured.err
    assert LOG_FILE.exists()
    with LOG_FILE.open() as f:
        lines = f.readlines()
    assert test_message in lines[-1]


def test_info(capsys, test_logger, test_message):
    test_logger.info(test_message)
    captured = capsys.readouterr()
    assert test_message in captured.err
    assert LOG_FILE.exists()
    with LOG_FILE.open() as f:
        lines = f.readlines()
    assert test_message in lines[-1]


def test_warn(capsys, test_logger, test_message):
    test_logger.warn(test_message)
    captured = capsys.readouterr()
    assert test_message in captured.err
    assert LOG_FILE.exists()
    with LOG_FILE.open() as f:
        lines = f.readlines()
    assert test_message in lines[-1]


def test_warning(capsys, test_logger, test_message):
    test_logger.warning(test_message)
    captured = capsys.readouterr()
    assert test_message in captured.err
    assert LOG_FILE.exists()
    with LOG_FILE.open() as f:
        lines = f.readlines()
    assert test_message in lines[-1]


def test_error(capsys, test_logger, test_message):
    test_logger.error(test_message)
    captured = capsys.readouterr()
    assert test_message in captured.err
    assert LOG_FILE.exists()
    with LOG_FILE.open() as f:
        lines = f.readlines()
    assert test_message in lines[-1]


def test_critical(capsys, test_logger, test_message):
    test_logger.critical(test_message)
    captured = capsys.readouterr()
    assert test_message in captured.err
    assert LOG_FILE.exists()
    with LOG_FILE.open() as f:
        lines = f.readlines()
    assert test_message in lines[-1]


def test_logger_with_custom_log_filepath(capsys, test_logger_with_custom_filepath, test_message):
    test_logger_with_custom_filepath.info(test_message)
    captured = capsys.readouterr()
    assert test_message in captured.err
    assert LOG_FILE.exists()
    with LOG_FILE.open() as f:
        lines = f.readlines()
    assert test_message in lines[-1]
