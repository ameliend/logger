from __future__ import annotations

import logging
import subprocess
import sys
import traceback
import warnings
from datetime import date
from pathlib import Path
from platform import system as platform_name
from tempfile import gettempdir

from logger import colors

CURRENT_DIR = Path(__file__).parent

TOASTER = CURRENT_DIR / "resources" / "toast64.exe"
ICONS_PATH = CURRENT_DIR / "resources" / "icons"
LOG_FILE = Path(gettempdir()) / f"logger_{date.today()}.log"

# CUSTOM LEVELS
logging.TRACE = 5
logging.DONE = 25
logging.SUCCESS = 35
logging.addLevelName(logging.TRACE, "TRACE")
logging.addLevelName(logging.DONE, "DONE")
logging.addLevelName(logging.SUCCESS, "SUCCESS")


class CustomFormatter(logging.Formatter):
    """A custom log formatter that formats and colors log messages based on their severity.

    It is used in the logging StreamHandler for the custom Logger class.

    Attributes
    ----------
        LINE_FORMAT : str
            The default line format for a log message.
        TRACE_LINE_FORMAT : str
            The line format used when logging at the "TRACE" level, including custom fields.
        COLOR_MAPPING : dict
            A mapping of log levels to ANSI escape codes for colored output in a terminal.
    """

    _LINE_FORMAT_START = "%(asctime)s :: %(name)s"
    _LINE_FORMAT_END = "%(levelname)s :: %(message)s"
    LINE_FORMAT = f"{_LINE_FORMAT_START} :: {_LINE_FORMAT_END}"
    TRACE_LINE_FORMAT = f"{_LINE_FORMAT_START} :: %(custom1)s:%(custom2)s:%(custom3)d :: {_LINE_FORMAT_END}"
    COLOR_MAPPING = {
        logging.TRACE: colors.BG_DARKGRAY,
        logging.DEBUG: colors.BG_MAGENTA,
        logging.INFO: colors.BG_BLUE,
        logging.DONE: colors.BG_BLUE,
        logging.WARNING: f"{colors.BG_YELLOW}{colors.BLACK}",
        logging.SUCCESS: f"{colors.BG_GREEN}{colors.BLACK}",
        logging.ERROR: f"{colors.BG_RED}{colors.BLACK}",
        logging.CRITICAL: f"{colors.BG_RED}{colors.BLACK}",
    }

    def format(self, record: logging.LogRecord) -> str:
        """Format a log record into a string with colored output based on the severity of the message.

        Parameters
        ----------
        record : logging.LogRecord
            The log record to be formatted.

        Returns
        -------
        str
            A formatted and colored log message.
        """
        if record.levelno == logging.TRACE:
            log_fmt = f"{self.COLOR_MAPPING.get(record.levelno)}{self.TRACE_LINE_FORMAT}{colors.END}"
        else:
            log_fmt = f"{self.COLOR_MAPPING.get(record.levelno)}{self.LINE_FORMAT}{colors.END}"
        formatter = logging.Formatter(log_fmt, datefmt="%H:%M:%S")
        return formatter.format(record)


class CustomFileFormatter(logging.Formatter):
    """A custom log formatter that formats log messages based on their severity.

    It is used in the logging FileHandler for the custom Logger class.

    Attributes
    ----------
        LINE_FORMAT : str
            The default line format for a log message.
        TRACE_LINE_FORMAT : str
            The line format used when logging at the "TRACE" level, including custom fields.
    """

    _LINE_FORMAT_START = "%(asctime)s :: %(name)s"
    _LINE_FORMAT_END = "%(levelname)s :: %(message)s"
    LINE_FORMAT = f"{_LINE_FORMAT_START} :: {_LINE_FORMAT_END}"
    TRACE_LINE_FORMAT = f"{_LINE_FORMAT_START} :: %(module)s:%(funcName)s:%(lineno)d :: {_LINE_FORMAT_END}"

    def format(self, record: logging.LogRecord) -> str:
        """Format a log record into a string with file information based on the severity of the message.

        Parameters
        ----------
        record : logging.LogRecord
            The log record to be formatted.

        Returns
        -------
        str
            A formatted log message.
        """
        log_fmt = self.TRACE_LINE_FORMAT if record.levelno == logging.TRACE else self.LINE_FORMAT
        formatter = logging.Formatter(log_fmt, datefmt="%H:%M:%S")
        return formatter.format(record)


class Logger(logging.Logger):
    """A custom logger that logs messages to both console and file with custom formatters.

    Also supports desktop notifications on Windows platform.

    Parameters
    ----------
    name : str, optional
        The name of the logger instance, default is "default".
    level : int | str, optional
        Logging level threshold, can be a string or an integer value, default is "TRACE".
    log_file : str, optional
        Path to the log file where messages will be logged, default is `LOG_FILE`.

    Attributes
    ----------
    log_file : str
        The path of the log file where messages will be logged.

    See Also
    --------
        CustomFormatter : Used for formatting console logs.
        CustomFileFormatter : Used for formatting file logs.
    """

    def __init__(self, name: str = "default", level: int | str = "TRACE", log_file: str = LOG_FILE) -> None:
        super().__init__(name)
        self.log_file = log_file
        self.setLevel(level)
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(CustomFormatter())
        self.addHandler(console_handler)
        file_handler = logging.FileHandler(filename=log_file, encoding="utf8")
        file_handler.setFormatter(CustomFileFormatter())
        self.addHandler(file_handler)

    @staticmethod
    def notify_toaster(
        title: str,
        message: str,
        application: str = "Logger",
        icon: str | None = None,
        activation_argument: str | None = None,
        actions: list | None = None,
    ) -> None:
        """Send a desktop notification using the `toast64.exe` executable in resouces folder.

        Parameters
        ----------
        title : str
            The title of the desktop notification.
        message : str
            The content of the desktop notification.
        application : str, optional
            The name of the application that sends the notification, default is "Logger".
        icon : str | None, optional
            Path to an icon file for the desktop notification, by default None.
        activation_argument : str | None, optional
            Additional argument passed to the application when clicking on the notification, by default None.
        actions : list[dict] | None, optional
            A list of dictionaries specifying additional actions that can be performed in response
            to the desktop notification, each dictionary should contain "name" and "activation_argument",
            by default None.
        """
        if platform_name() != "Windows":
            return
        command = [TOASTER, "--app-id", application, "--title", title, "--message", message]
        if icon:
            command += ["--icon", icon]
        if activation_argument:
            command += ["--activation-arg", activation_argument]
        if actions:
            for action in actions:
                if action.get("name") and action.get("activation_argument"):
                    command += ["--action", action["name"], "--action-arg", action["activation_argument"]]
        subprocess.run(command, check=False)

    def _log(
        self,
        level,
        msg,
        args,
        exc_info=None,
        extra=None,
        stack_info=False,
        stacklevel=1,
        notify=False,
    ) -> None:
        super()._log(level, msg, args, exc_info, extra, stack_info, stacklevel)
        if notify:
            level_name = logging.getLevelName(level)
            self.notify_toaster(
                title=level_name,
                message=msg % args,
                icon=str(ICONS_PATH / f"{level_name.lower()}.png"),
                activation_argument=self.log_file,
            )

    def trace(self, msg, *args, notify=False, **kwargs) -> None:
        """Log 'msg % args' with severity 'TRACE'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.trace("Houston, we have a %s", "thorny problem", exc_info=1)

        Parameters
        ----------
        msg : str
            The message to log. Can include placeholders for args which are formatted into this string.
        *args
            Variable length argument list that is used in conjunction with the format specifier 'msg'.
        notify : bool, optional
            If True and running on a Windows platform, it will display a desktop notification.
            Defaults to False.
        **kwargs: dict, optional
            Any additional keyword arguments are passed through to underlying logging method.
        """
        if self.isEnabledFor(logging.TRACE):
            frame = sys._getframe(1)  # noqa: SLF001
            module = Path(frame.f_code.co_filename).stem
            function = frame.f_code.co_name
            line_number = frame.f_lineno
            extra_args = {"custom1": module, "custom2": function, "custom3": line_number}
            self._log(logging.TRACE, msg, args, notify=notify, extra=extra_args, **kwargs)

    def debug(self, msg, *args, notify=False, **kwargs) -> None:
        """Log 'msg % args' with severity 'DEBUG'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.debug("Houston, we have a %s", "thorny problem", exc_info=1)

        Parameters
        ----------
        msg : str
            The message to log. Can include placeholders for args which are formatted into this string.
        *args
            Variable length argument list that is used in conjunction with the format specifier 'msg'.
        notify : bool, optional
            If True and running on a Windows platform, it will display a desktop notification.
            Defaults to False.
        **kwargs: dict, optional
            Any additional keyword arguments are passed through to underlying logging method.
        """
        if self.isEnabledFor(logging.DEBUG):
            self._log(logging.DEBUG, msg, args, notify=notify, **kwargs)

    def info(self, msg, *args, notify=False, **kwargs) -> None:
        """Log 'msg % args' with severity 'INFO'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.info("Houston, we have a %s", "interesting problem", exc_info=1)

        Parameters
        ----------
        msg : str
            The message to log. Can include placeholders for args which are formatted into this string.
        *args
            Variable length argument list that is used in conjunction with the format specifier 'msg'.
        notify : bool, optional
            If True and running on a Windows platform, it will display a desktop notification.
            Defaults to False.
        **kwargs: dict, optional
            Any additional keyword arguments are passed through to underlying logging method.
        """
        if self.isEnabledFor(logging.INFO):
            self._log(logging.INFO, msg, args, notify=notify, **kwargs)

    def done(self, msg, *args, notify=False, **kwargs) -> None:
        """Log 'msg % args' with severity 'DONE'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.done("Houston, we have a %s", "interesting problem", exc_info=1)

        Parameters
        ----------
        msg : str
            The message to log. Can include placeholders for args which are formatted into this string.
        *args
            Variable length argument list that is used in conjunction with the format specifier 'msg'.
        notify : bool, optional
            If True and running on a Windows platform, it will display a desktop notification.
            Defaults to False.
        **kwargs: dict, optional
            Any additional keyword arguments are passed through to underlying logging method.
        """
        if self.isEnabledFor(logging.DONE):
            self._log(logging.DONE, msg, args, notify=notify, **kwargs)

    def warning(self, msg, *args, notify=False, **kwargs) -> None:
        """Log 'msg % args' with severity 'WARNING'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.warning("Houston, we have a %s", "bit of a problem", exc_info=1)

        Parameters
        ----------
        msg : str
            The message to log. Can include placeholders for args which are formatted into this string.
        *args
            Variable length argument list that is used in conjunction with the format specifier 'msg'.
        notify : bool, optional
            If True and running on a Windows platform, it will display a desktop notification.
            Defaults to False.
        **kwargs: dict, optional
            Any additional keyword arguments are passed through to underlying logging method.
        """
        if self.isEnabledFor(logging.WARNING):
            self._log(logging.WARNING, msg, args, notify=notify, **kwargs)

    def warn(self, msg, *args, notify=False, **kwargs) -> None:
        """Log 'msg % args' with severity 'WARNING'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.warning("Houston, we have a %s", "bit of a problem", exc_info=1)

        Parameters
        ----------
        msg : str
            The message to log. Can include placeholders for args which are formatted into this string.
        *args
            Variable length argument list that is used in conjunction with the format specifier 'msg'.
        notify : bool, optional
            If True and running on a Windows platform, it will display a desktop notification.
            Defaults to False.
        **kwargs: dict, optional
            Any additional keyword arguments are passed through to underlying logging method.
        """
        warnings.warn(
            "The 'warn' method is deprecated, use 'warning' instead",
            DeprecationWarning,
            stacklevel=2,
        )
        self.warning(msg, *args, notify=notify, **kwargs)

    def success(self, msg, *args, notify=False, **kwargs) -> None:
        """Log 'msg % args' with severity 'SUCCESS'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.success("Houston, we have a %s", "bit of a problem", exc_info=1)

        Parameters
        ----------
        msg : str
            The message to log. Can include placeholders for args which are formatted into this string.
        *args
            Variable length argument list that is used in conjunction with the format specifier 'msg'.
        notify : bool, optional
            If True and running on a Windows platform, it will display a desktop notification.
            Defaults to False.
        **kwargs: dict, optional
            Any additional keyword arguments are passed through to underlying logging method.
        """
        if self.isEnabledFor(logging.SUCCESS):
            self._log(logging.SUCCESS, msg, args, notify=notify, **kwargs)

    def error(self, msg, *args, notify=False, **kwargs) -> None:
        """Log 'msg % args' with severity 'ERROR'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.error("Houston, we have a %s", "major problem", exc_info=1)

        Parameters
        ----------
        msg : str
            The message to log. Can include placeholders for args which are formatted into this string.
        *args
            Variable length argument list that is used in conjunction with the format specifier 'msg'.
        notify : bool, optional
            If True and running on a Windows platform, it will display a desktop notification.
            Defaults to False.
        **kwargs: dict, optional
            Any additional keyword arguments are passed through to underlying logging method.
        """
        if self.isEnabledFor(logging.ERROR):
            self._log(logging.ERROR, msg, args, notify=notify, **kwargs)

    def exception(self, msg, *args, exc_info=True, notify=False, **kwargs) -> None:
        """Convenience method for logging an ERROR with exception information.

        Parameters
        ----------
        msg : str
            The message to log. Can include placeholders for args which are formatted into this string.
        *args
            Variable length argument list that is used in conjunction with the format specifier 'msg'.
        exc_info : bool, optional
            If True, exception information is added to the logging message. Defaults to True.
        notify : bool, optional
            If True and running on a Windows platform, it will display a desktop notification.
            Defaults to False.
        **kwargs: dict, optional
            Any additional keyword arguments are passed through to underlying logging method.
        """  # noqa: D401
        self.error(msg, *args, exc_info=exc_info, notify=notify, **kwargs)

    def critical(self, msg, *args, notify=False, **kwargs) -> None:
        """Log 'msg % args' with severity 'CRITICAL'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.critical("Houston, we have a %s", "major disaster", exc_info=1)

        Parameters
        ----------
        msg : str
            The message to log. Can include placeholders for args which are formatted into this string.
        *args
            Variable length argument list that is used in conjunction with the format specifier 'msg'.
        notify : bool, optional
            If True and running on a Windows platform, it will display a desktop notification.
            Defaults to False.
        **kwargs: dict, optional
            Any additional keyword arguments are passed through to underlying logging method.
        """
        if self.isEnabledFor(logging.CRITICAL):
            self._log(logging.CRITICAL, msg, args, notify=notify, **kwargs)

    fatal = critical

    def log(self, level, msg, *args, notify=False, **kwargs) -> None:
        """Log 'msg % args' with the integer severity 'level'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.log(level, "We have a %s", "mysterious problem", exc_info=1)

        Parameters
        ----------
        level : int
            The severity level of the log message.
        msg : str
            The message to log. Can include placeholders for args which are formatted into this string.
        *args
            Variable length argument list that is used in conjunction with the format specifier 'msg'.
        notify : bool, optional
            If True and running on a Windows platform, it will display a desktop notification.
            Defaults to False.
        **kwargs: dict, optional
            Any additional keyword arguments are passed through to underlying logging method.

        Raises
        ------
        TypeError
            If the level is not an integer. This exception is raised if `logging.raiseExceptions` is True.
        """
        if not isinstance(level, int):
            if logging.raiseExceptions:
                msg = "level must be an integer"
                raise TypeError(msg)
            return
        if self.isEnabledFor(level):
            self._log(level, msg, args, notify=notify, **kwargs)


_root_logger = Logger(name="root")


def _log_unhandled_exception(exc_type, exc_value, exc_traceback) -> None:
    if issubclass(exc_type, KeyboardInterrupt):
        return
    for line in traceback.format_exception(exc_type, exc_value, exc_traceback):
        _root_logger.error(line.strip())


sys.excepthook = _log_unhandled_exception
