import webbrowser

from logger.main import LOG_FILE


def open_last_log_file() -> None:
    """Open the last logger default file saved in TEMP."""
    webbrowser.open(LOG_FILE)
