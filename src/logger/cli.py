"""Console script for Logger."""

import argparse

from logger.utils import open_last_log_file


def main_cli() -> None:
    """Console script for Logger."""
    parser = argparse.ArgumentParser(description="Logger command-line interface.")
    parser.add_argument("--last-log-file", action="store_true", help="Open the last log file.")
    args = parser.parse_args()
    if args.last_log_file:
        open_last_log_file()
