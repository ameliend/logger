"""Top-level package for Logger."""

from logger._version import __version__
from logger.main import Logger, _root_logger

__author__ = """Amelien Deshams"""
__email__ = "a.deshams+git@slmail.me"
__all__ = [
    "Logger",
    "__version__",
    "critical",
    "debug",
    "done",
    "error",
    "exception",
    "info",
    "setLevel",
    "success",
    "trace",
    "warn",
    "warning",
]

setLevel = _root_logger.setLevel  # noqa: N816
trace = _root_logger.trace
debug = _root_logger.debug
info = _root_logger.info
done = _root_logger.done
warn = _root_logger.warn
warning = _root_logger.warning
success = _root_logger.success
error = _root_logger.error
critical = _root_logger.critical
exception = _root_logger.exception
