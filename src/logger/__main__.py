"""Entry point for Logger."""

from logger.cli import main_cli

if __name__ == "__main__":
    main_cli()
